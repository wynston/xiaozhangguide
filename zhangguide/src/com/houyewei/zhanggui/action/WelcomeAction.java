package com.houyewei.zhanggui.action;

import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Persistent;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.houyewei.zhanggui.PMF;
import com.houyewei.zhanggui.pojo.Money;
import com.houyewei.zhanggui.pojo.MyUser;

public class WelcomeAction extends ActionSupport {

	/**@author wynston
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String href;
	private UserService userService = UserServiceFactory.getUserService();
	private String email;
	private String username;
	public String execute() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		HttpServletRequest request = ServletActionContext.getRequest();
		Logger  logger = Logger.getLogger("Welcome");
		if(userService.isUserLoggedIn()){
			User user=userService.getCurrentUser();
			//logger.info(userService.getCurrentUser().toString());
			System.out.println(user.getEmail());
			MyUser myuser=new MyUser();
			myuser.setUser(user);
			this.setEmail(user.getEmail());
			this.setUsername(user.getNickname());
			Money m=new Money();
			m.setColor("blue");
			try {
			pm.makePersistent(myuser);
		       pm.makePersistent(m);
		    } finally {
		        pm.close();
		    }
		}else{
			System.out.println(request.getRequestURI());
			href=userService.createLoginURL(request.getRequestURI());
			System.out.println(href);
			logger.info(href);
			return "INPUT";
		}
		return "SUCCESS";
 
	}
	

	public String getHref() {
		return href;
	}

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public void setHref(String href) {
		this.href = href;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}

}
